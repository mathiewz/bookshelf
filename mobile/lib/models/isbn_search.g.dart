// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'isbn_search.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

IsbnSearch _$IsbnSearchFromJson(Map<String, dynamic> json) => IsbnSearch(
      auteur: json['auteur'] == null
          ? null
          : Auteur.fromJson(json['auteur'] as Map<String, dynamic>),
      titre: json['titre'] as String?,
      edition: json['edition'] as String?,
      prix: (json['prix'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$IsbnSearchToJson(IsbnSearch instance) =>
    <String, dynamic>{
      'auteur': instance.auteur?.toJson(),
      'titre': instance.titre,
      'edition': instance.edition,
      'prix': instance.prix,
    };
