import 'package:json_annotation/json_annotation.dart';

part 'tag.g.dart';

@JsonSerializable(explicitToJson: true)
class Tag {
  late int id;
  late String label;
  late String icon;
  late int iconCode;
  late String color;

  Tag(this.id, this.label, this.icon, this.iconCode, this.color);

  factory Tag.fromJson(Map<String, dynamic> json) => _$TagFromJson(json);
  Map<String, dynamic> toJson() => _$TagToJson(this);
}