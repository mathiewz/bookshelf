// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tag.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Tag _$TagFromJson(Map<String, dynamic> json) => Tag(
      json['id'] as int,
      json['label'] as String,
      json['icon'] as String,
      json['iconCode'] as int,
      json['color'] as String,
    );

Map<String, dynamic> _$TagToJson(Tag instance) => <String, dynamic>{
      'id': instance.id,
      'label': instance.label,
      'icon': instance.icon,
      'iconCode': instance.iconCode,
      'color': instance.color,
    };
