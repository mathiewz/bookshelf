import 'package:json_annotation/json_annotation.dart';

part 'auteur.g.dart';

@JsonSerializable(explicitToJson: true)
class Auteur {
  final int? id;
  final String nom;
  final String? prenom;


  Auteur({this.id, required this.nom, this.prenom});

  factory Auteur.fromJson(Map<String, dynamic> json) => _$AuteurFromJson(json);
  Map<String, dynamic> toJson() => _$AuteurToJson(this);

  String getDisplayName() {
    if (prenom != null) {
      return '$prenom $nom';
    } else {
      return nom;
    }
  }
}
