import 'package:flutter/material.dart';

class LoadingAppBar extends StatelessWidget implements PreferredSizeWidget{

  final bool loading;
  final Widget title;
  final List<Widget>? actions;

  late AppBar appBar;

  LoadingAppBar({super.key, required this.loading, required this.title, this.actions}) {
    appBar = AppBar(
      title: title,
      actions: actions,
      bottom: PreferredSize(
        preferredSize: const Size.fromHeight(6.0),
        child: loading
            ? const LinearProgressIndicator(value: null)
            : Container(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return appBar;
  }

  @override
  Size get preferredSize => appBar.preferredSize;


}