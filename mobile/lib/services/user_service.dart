import 'package:bookshelf/http/backend_http_client.dart';
import 'package:bookshelf/models/livre.dart';

class UserService{

  Future<List<Livre>> getUserBooks() async {
    var response = await backend.get('user/books');
    var rawList = response.data as List;
    return rawList.map((json) => Livre.fromJson(json)).toList();
  }


}