import 'dart:convert';

import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:bookshelf/http/backend_http_client.dart';
import 'package:bookshelf/models/user.dart';

class AuthService {

  Future<String?> loadToken() async {
    var prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('token');
    if (token == null) {
      return null;
    }

    if (JwtDecoder.getExpirationDate(token).isBefore(DateTime.now())) {
      print('Token expired, relogin...');
      logout();
      await login(prefs.getString('user')!, prefs.getString('hashedPwd')!);
      token = prefs.getString('token');
    }

    return token;
  }

  login(String user, String hashedPassword) async {
    var body = jsonEncode({
      'username': user,
      'password': hashedPassword
    });

    var res = await backend.post('login', data: body);
    var token = res.data!;
    var prefs = await SharedPreferences.getInstance();

    prefs.setString('user', user);
    prefs.setString('hashedPwd', hashedPassword);
    prefs.setString('token', token);
  }

  logout() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.remove('user');
    prefs.remove('hashedPwd');
    prefs.remove('token');
  }

  getUser() async {
    var token = await loadToken();
    if(token == null){
      return null;
    }
    var claims = JwtDecoder.decode(token);
    return User(claims['id'], claims['username']);
  }

}
