import 'package:bookshelf/models/livre.dart';
import 'package:bookshelf/models/tag.dart';
import 'package:bookshelf/pages/livre_view.dart';
import 'package:bookshelf/services/auth_service.dart';
import 'package:bookshelf/services/livre_service.dart';
import 'package:bookshelf/services/param_service.dart';
import 'package:bookshelf/validators.dart';
import 'package:bookshelf/widget/TextDivider.dart';
import 'package:bookshelf/widget/card_padding.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../models/auteur.dart';

const digitsWithOptionalTwoDecimals = "([0-9]+(.[0-9]{0,2})?)";

class BookFormPage extends StatefulWidget {
  final Livre? livre;

  const BookFormPage({super.key, this.livre});

  @override
  State<BookFormPage> createState() => BookFormPageState();
}

class BookFormPageState extends State<BookFormPage>
    with SingleTickerProviderStateMixin {
  final ParamsService _paramsService = ParamsService();

  late TabController _tabController;

  bool _loading = false;
  late bool _editMode;

  set loading(bool value) {
    setState(() {
      _loading = value;
    });
  }

  final TextEditingController _isbnController = TextEditingController();
  final TextEditingController _titreController = TextEditingController();
  final TextEditingController _nomController = TextEditingController();
  final TextEditingController _prenomController = TextEditingController();
  final TextEditingController _editionController = TextEditingController();
  final TextEditingController _prixController = TextEditingController();
  bool _pret = false;
  bool _add = false;

  List<SelectableTag> tags = [];

  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    _tabController.dispose();
    _isbnController.dispose();
    _titreController.dispose();
    _nomController.dispose();
    _prenomController.dispose();
    _editionController.dispose();
    _prixController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    _paramsService.getTags().then((value) => setState(() {
          tags = value.map((e) => SelectableTag(tag: e, selected: false)).toList();
          if (widget.livre != null) {
            markFormTagAsSelected(tag) => tags.firstWhere((e) => e.tag.id == tag.id).selected = true;
            widget.livre!.tags.forEach(markFormTagAsSelected);
          }
        }));
    if (widget.livre != null) {
      _editMode = true;
      var livre = widget.livre!;
      _titreController.text = livre.titre;
      _nomController.text = livre.auteur.nom;
      _prenomController.text = livre.auteur.prenom ?? '';
      _editionController.text = livre.edition;
      _prixController.text = livre.prix.toStringAsFixed(2);
      _pret = livre.pret;
    } else {
      _editMode = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_editMode) {
      return Scaffold(
        appBar: AppBar(
          title: const Text("Edition livre"),
        ),
        body: formBody(),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text("Nouveau livre"),
        bottom: TabBar(
          controller: _tabController,
          tabs: const <Widget>[
            Tab(icon: Icon(Icons.qr_code_scanner)),
            Tab(icon: Icon(Icons.edit_note)),
          ],
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[
          scanBody(),
          formBody(),
        ],
      ),
    );
  }

  Widget scanBody() {
    return Form(
        child: Center(
            child: CardPadding(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          FilledButton.tonal(
            onPressed: scanBarcode,
            child: const Wrap(children: [
              Icon(Icons.camera_alt),
              Text(" Scanner un code barre")
            ]),
          ),
          const SizedBox(height: 16),
          const TextDivider(label: " ou "),
          const SizedBox(height: 16),
          TextFormField(
            enabled: !_loading,
            controller: _isbnController,
            textInputAction: TextInputAction.search,
            onFieldSubmitted: (_) => findIsbn(_isbnController.text),
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'ISBN',
            ),
          ),
          const SizedBox(height: 8),
          FilledButton.tonal(
            onPressed: () => findIsbn(_isbnController.text),
            child: const Text("Rechercher un code ISBN"),
          ),
        ],
      ),
    )));
  }

  scanBarcode() async {
    String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode("#ff6666", "Annuler", false, ScanMode.BARCODE);
    if(barcodeScanRes != "-1"){
      findIsbn(barcodeScanRes);
    }
  }

  findIsbn(String isbn) async {
    try {
      var livre = await LivreService().findByISBN(isbn);
      _titreController.text = livre.titre ?? '';
      _nomController.text = livre.auteur?.nom ?? '';
      _prenomController.text = livre.auteur?.prenom ?? '';
      _editionController.text = livre.edition ?? '';
      _prixController.text = livre.prix?.toStringAsFixed(2) ?? '';
      _tabController.animateTo(1);
    } on DioException catch (e) {
      if (e.response?.statusCode == 404) {
        toaster("Aucune correspondance trouvée 😢");
      } else {
        toaster("Une erreur est survenue 😢");
      }
    }
  }

  Widget formBody() {
    var titleField = TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      enabled: !_loading,
      controller: _titreController,
      decoration: const InputDecoration(labelText: 'Titre'),
      textInputAction: TextInputAction.next,
      validator: requiredField,
    );

    var nomField = TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      enabled: !_loading,
      controller: _nomController,
      decoration: const InputDecoration(labelText: 'Nom'),
      textInputAction: TextInputAction.next,
      validator: requiredField,
    );

    var prenomField = TextFormField(
      enabled: !_loading,
      controller: _prenomController,
      decoration: const InputDecoration(labelText: 'Prenom'),
      textInputAction: TextInputAction.next,
    );

    var editionField = TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      enabled: !_loading,
      controller: _editionController,
      decoration: const InputDecoration(labelText: 'Edition'),
      textInputAction: TextInputAction.next,
      validator: requiredField,
    );

    var prixField = TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      enabled: !_loading,
      controller: _prixController,
      keyboardType: TextInputType.number,
      inputFormatters: [
        FilteringTextInputFormatter.allow(RegExp(digitsWithOptionalTwoDecimals))
      ],
      decoration: const InputDecoration(
          labelText: 'Prix', suffixIcon: Icon(Icons.euro)),
      textInputAction: TextInputAction.next,
      validator: requiredField,
    );

    final MaterialStateProperty<Icon?> checkIcon =
        MaterialStateProperty.resolveWith<Icon?>(
      (Set<MaterialState> states) {
        if (states.contains(MaterialState.selected)) {
          return const Icon(Icons.check);
        }
        return const Icon(Icons.close);
      },
    );

    List<Widget> actions = [];

    var pretField = SwitchListTile(
      thumbIcon: checkIcon,
      title: const Text('Marquer comme prêté'),
      value: _pret,
      onChanged: (bool value) => setState(() {
        _pret = value;
      }),
      secondary: const Icon(Icons.volunteer_activism),
    );
    actions.add(pretField);

    if (!_editMode) {
      var addField = SwitchListTile(
        thumbIcon: checkIcon,
        title: const Text('Ajouter à ma bibliothèque'),
        value: _add,
        onChanged: (bool value) => setState(() {
          _add = value;
        }),
        secondary: const Icon(Icons.shelves),
      );
      actions.add(addField);
    }

    return SingleChildScrollView(
      child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              CardPadding(
                title: "Titre",
                titlePadding: 0,
                child: titleField,
              ),
              CardPadding(
                title: "Auteur",
                titlePadding: 0,
                child: Column(children: [nomField, prenomField]),
              ),
              CardPadding(
                title: "Edition",
                titlePadding: 0,
                child: Column(children: [editionField, prixField]),
              ),
              CardPadding(
                title: "Etiquettes",
                child: tagFields(),
              ),
              CardPadding(
                child: Column(children: actions),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(4, 4, 4, 60),
                child: FilledButton.tonal(
                  onPressed: _loading ? null : () => onSubmit(),
                  child: _editMode
                      ? const Text('Enregistrer les modifications')
                      : const Text('Ajouter un livre'),
                ),
              )
            ],
          )),
    );
  }

  tagFields() {
    var inputs = tags
        .map((tag) => ChoiceChip(
            showCheckmark: false,
            avatar: Icon(IconDataSolid(tag.tag.iconCode),
                color: Theme.of(context).colorScheme.primary),
            label: Text(tag.tag.label),
            selected: tag.selected,
            onSelected: (bool selected) {
              setState(() {
                tag.selected = selected;
              });
            }))
        .toList();
    return Wrap(spacing: 8, runSpacing: 8, children: inputs);
  }

  onSubmit() async {
    if (!_formKey.currentState!.validate()) {
      toaster('Formulaire invalide !');
      return;
    }

    loading = true;

    try {
      var user = await AuthService().getUser();
      var livre = Livre(
          auteur:
              Auteur(nom: _nomController.text, prenom: _prenomController.text),
          titre: _titreController.text,
          edition: _editionController.text,
          prix: double.parse(_prixController.text),
          detenteurs: _add ? [user] : [],
          tags: tags
              .where((input) => input.selected)
              .map((input) => input.tag)
              .toList(),
          pret: _pret,
          lu: false);

      if (_editMode) {
        livre.id = widget.livre!.id;
        livre.detenteurs = widget.livre!.detenteurs;
        LivreService().edit(livre).then((_) => Navigator.pop(context));
      } else {
        LivreService().add(livre).then((value) {
          Navigator.pop(context);
          return Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => BookViewPage(livreId: value.id!)));
        });
      }
    } on Exception catch (e) {
      dialog("Une erreur est survenue : $e");
    } finally {
      loading = false;
    }
  }

  void toaster(String label) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        behavior: SnackBarBehavior.floating,
        content: Text(label),
        showCloseIcon: true,
      ),
    );
  }

  dialog(String text) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          alignment: Alignment.center,
          content: Text(text),
        );
      },
    );
  }
}

class SelectableTag {
  final Tag tag;
  bool selected;

  SelectableTag({required this.tag, required this.selected});
}
