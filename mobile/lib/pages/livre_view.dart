import 'package:bookshelf/consts.dart';
import 'package:bookshelf/models/livre.dart';
import 'package:bookshelf/models/tag.dart';
import 'package:bookshelf/models/user.dart';
import 'package:bookshelf/pages/livre_form.dart';
import 'package:bookshelf/services/auth_service.dart';
import 'package:bookshelf/services/livre_service.dart';
import 'package:bookshelf/widget/card_padding.dart';
import 'package:bookshelf/widget/loading_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class BookViewPage extends StatefulWidget {
  final String livreId;

  const BookViewPage({super.key, required this.livreId});

  @override
  State<BookViewPage> createState() => BookViewPageState();
}

class BookViewPageState extends State<BookViewPage> {
  late Future<Livre> _futureLivre;
  Livre? _livre;

  final livreService = LivreService();
  final authService = AuthService();

  bool loading = true;
  List<Widget> menu = [];

  set futureLivre(Future<Livre> futureLivre) {
    loading = true;
    setState(() {
      _futureLivre = futureLivre.then((value) {
        setState(() {
          _livre = value;
          refreshMenu(value);
        });
        return value;
      }).whenComplete(() => setState(() {
            loading = false;
          }));
    });
  }

  @override
  void initState() {
    super.initState();
    futureLivre = livreService.fetchLivre(widget.livreId);
  }

  @override
  Widget build(BuildContext context) {
    var futureBuilder = FutureBuilder<Livre>(
      future: _futureLivre,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return buildLoaded(context, snapshot.data!);
        }
        if (snapshot.hasError) {
          return Container(
              margin: defaultSpacing, child: Text(snapshot.error.toString()));
        }
        return Container();
      },
    );

    return Scaffold(
      appBar: LoadingAppBar(
        title: const Text("Détails"),
        loading: loading,
        actions: menu,
      ),
      body: futureBuilder,
      floatingActionButton: _livre == null ? null : FloatingActionButton(
        onPressed: () => goToEditPage(),
        child: const Icon(Icons.edit),
      ),
    );
  }

  Widget buildLoaded(BuildContext context, Livre livre) {
    List<Widget> children = [];

    children.add(buildTitle(livre, context));
    children.add(buildChips(livre));
    children.add(buildDetailsText(livre));

    return RefreshIndicator(
        onRefresh: () {
          var future = futureLivre = livreService.fetchLivre(widget.livreId);
          loading = false;
          return future;
        },
        child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(children: children)));
  }

  Widget buildTitle(Livre livre, BuildContext context) {
    var title = Text(
      livre.titre,
      style: Theme.of(context).textTheme.headlineMedium,
    );
    return CardPadding(child: title);
  }

  Widget buildChips(Livre livre) {
    var tags = livre.tags;

    if (livre.lu) {
      Tag tag = Tag(0, 'Lu', '', 0xf00c, '');
      tags = [tag] + tags;
    }
    if (tags.isEmpty) {
      return Container();
    }

    var chips = tags
        .map((tag) => Chip(avatar: Icon(IconDataSolid(tag.iconCode)), label: Text(tag.label)))
        .map((chip) =>
            Container(margin: const EdgeInsets.only(right: 16), child: chip))
        .toList();

    return CardPadding(
        child: SingleChildScrollView(
            scrollDirection: Axis.horizontal, child: Row(children: chips)));
  }

  Widget buildDetailsText(Livre livre) {
    var auteur = Text.rich(TextSpan(
        text: "écrit par ", children: [bold(livre.auteur.getDisplayName())]));

    var editions = Text.rich(
        TextSpan(text: "sorti aux éditions ", children: [bold(livre.edition)]));

    var prix = Text.rich(TextSpan(text: "\nil a couté ", children: [
      bold("${livre.prix.toStringAsFixed(2)}€"),
      const TextSpan(text: " à "),
      bold(livre.getOwners())
    ]));

    var pret = livre.pret
        ? const Text("\nIl est actuellement prété à quelqu'un.")
        : Container();

    var texts = CardPadding(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [auteur, editions, prix, pret],
    ));
    return texts;
  }

  TextSpan bold(String text) {
    return TextSpan(
        text: text, style: const TextStyle(fontWeight: FontWeight.bold));
  }

  refreshMenu(Livre livre) async {
    List<PopupMenuItem> actions = [];

    menuLabel(IconData icon, String label) => Row(children: [
          Padding(padding: const EdgeInsets.only(right: 8), child: Icon(icon)),
          Text(label)
        ]);

    if (livre.lu) {
      var markAsUnread = PopupMenuItem(
          child: menuLabel(Icons.close, "Marquer comme non-lu"),
          onTap: () => futureLivre = livreService.setRead(livre.id!, false));
      actions.add(markAsUnread);
    } else {
      var markAsRead = PopupMenuItem(
          child: menuLabel(Icons.check, "Marquer comme lu"),
          onTap: () => futureLivre = livreService.setRead(livre.id!, true));
      actions.add(markAsRead);
    }

    User user = await authService.getUser();
    if (livre.ownedBy(user)) {
      var removeFromBibliotheque = PopupMenuItem(
          child: menuLabel(
              Icons.remove_circle_outline, "Retirer de ma bibliothèque"),
          onTap: () =>
              futureLivre = livreService.removeFromUserBookshelf(livre.id!));
      actions.add(removeFromBibliotheque);
    } else {
      var addToBobliotheque = PopupMenuItem(
          child:
              menuLabel(Icons.add_circle_outline, "Ajouter a ma bibliothèque"),
          onTap: () =>
              futureLivre = livreService.addToUserBookshelf(livre.id!));
      actions.add(addToBobliotheque);
    }

    var edit = PopupMenuItem(
        child: menuLabel(Icons.edit, "Modifier"),
        onTap: () => goToEditPage());
    actions.add(edit);

    var delete = PopupMenuItem(
        child: menuLabel(Icons.delete, "Supprimer"),
        onTap: () => showDeletionPopup(livre));
    actions.add(delete);

    setState(() {
      menu = [PopupMenuButton(itemBuilder: (BuildContext context) => actions)];
    });
  }

  goToEditPage() {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => BookFormPage(livre: _livre)));
  }

  showDeletionPopup(Livre livre) {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text('Confirmer'),
        content: const Text(
            "La suppression du livre le supprimera pour l'ensemble des utilisateurs"),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: const Text('Annuler'),
          ),
          TextButton(
            onPressed: () {
              livreService.delete(livre.id!);
              Navigator.pop(context);
              Navigator.pop(context);
            },
            child: const Text('Confirmer'),
          ),
        ],
      ),
    );
  }
}
