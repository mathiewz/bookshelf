import 'dart:convert';

import 'package:bookshelf/main.dart';
import 'package:bookshelf/services/auth_service.dart';
import 'package:bookshelf/validators.dart';
import 'package:bookshelf/widget/card_padding.dart';
import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({super.key});

  @override
  State<LoginForm> createState() => LoginFormState();
}

class LoginFormState extends State<LoginForm> {
  bool loading = false;

  final authService = AuthService();

  final identifiantController = TextEditingController();
  final passwordController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    identifiantController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var idField = TextFormField(
      enabled: !loading,
      controller: identifiantController,
      decoration: const InputDecoration(labelText: 'Identifiant'),
      textInputAction: TextInputAction.next,
      validator: requiredField,
    );

    var passwordField = TextFormField(
      enabled: !loading,
      controller: passwordController,
      decoration: const InputDecoration(labelText: 'Mot de passe'),
      textInputAction: TextInputAction.go,
      onFieldSubmitted: (_) => onSubmit(context),
      validator: requiredField,
      obscureText: true,
    );

    var submitButton = TextButton(
        onPressed: loading ? null : () => onSubmit(context),
        child: const Text('Connexion'));

    var buttonRow = Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[submitButton]);

    var form = Form(
      key: _formKey,
      child: Column(
        children: <Widget>[idField, passwordField, buttonRow],
      ),
    );

    return Scaffold(
      body: Center(
        child: CardPadding(
          image: Image.asset('assets/img/book.jpg',fit: BoxFit.cover),
          child: form,
        ),
      ),
    );
  }

  Future<void> onSubmit(BuildContext context) async {
    if (!_formKey.currentState!.validate()) {
      dialog("Champs manquants");
      return;
    }

    setLoading(true);

    var user = identifiantController.text;
    var hashedPassword = sha512
        .convert(utf8.encode(passwordController.text + user.toUpperCase()))
        .toString();
    try {
      await login(user, hashedPassword);
    } on DioException catch (e) {
      String errorMessage = e.response?.statusCode == 401
          ? "Identifiants invalides"
          : "Une erreur est survenue lors de l'authentification : ${e.message}";
      dialog(errorMessage);
    } on Exception catch (e) {
      dialog("Une erreur est survenue lors de l'authentification : $e");
    } finally {
      setLoading(false);
    }
  }

  login(String user, String hashedPassword) async {
    await authService.login(user, hashedPassword);
    loadToken();
  }

  loadToken() {
    authService.loadToken().then((token) => {
      if (token != null) {
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const Launcher(userAuthenticated: true)))
      }
    });
  }

  setLoading(bool loading) {
    setState(() {
      this.loading = loading;
    });
  }

  dialog(String text) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Text(text),
        );
      },
    );
  }
}
