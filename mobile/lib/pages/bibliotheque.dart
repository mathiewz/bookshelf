import 'package:bookshelf/pages/livre_form.dart';
import 'package:flutter/material.dart';
import 'package:bookshelf/consts.dart';
import 'package:bookshelf/models/bibliotheque.dart';
import 'package:bookshelf/services/bookshelf_service.dart';
import 'package:bookshelf/widget/book_list.dart';
import 'package:bookshelf/widget/card_padding.dart';
import 'package:bookshelf/widget/loading_app_bar.dart';

class BibliothequePage extends StatefulWidget {
  const BibliothequePage({super.key});

  @override
  State<BibliothequePage> createState() => BibliothequePageState();
}

class BibliothequePageState extends State<BibliothequePage> {
  late Future<Bibliotheque> _futureBibliotheque;

  bool loading = true;
  final bibliothequeService = BibliothequeService();

  set futureBibliotheque(Future<Bibliotheque> futureBibliotheque) {
    setState(() {
      _futureBibliotheque = futureBibliotheque;
    });
  }

  @override
  void initState() {
    super.initState();

    futureBibliotheque =
        bibliothequeService.fetchBibliotheque().whenComplete(() => setState(() {
              loading = false;
            }));
  }

  @override
  Widget build(BuildContext context) {
    var futureBuilder = FutureBuilder<Bibliotheque>(
      future: _futureBibliotheque,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return buildLoaded(context, snapshot.data!);
        }
        if (snapshot.hasError) {
          return Container(
              margin: defaultSpacing, child: Text(snapshot.error.toString()));
        }
        return Container();
      },
    );

    return Scaffold(
      appBar: LoadingAppBar(
        title: const Text("Ma bibliothèque"),
        loading: loading,
      ),
      body: futureBuilder,
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.push(context,
            MaterialPageRoute(builder: (context) => const BookFormPage())),
        child: const Icon(Icons.add),
      ),
    );
  }

  Widget buildLoaded(BuildContext context, Bibliotheque bibliotheque) {
    List<Widget> texts = [];

    if (bibliotheque.nbLivres == 0) {
      texts.add(const Text("Hmm, c'est bien vide ici."));
      texts.add(const Text(
          "Pour ajouter des livres, on peut les chercher dans l'onglet rechercher"));
      texts.add(const Text(
          "Si ils ne sont pas encore référencés, on peut aussi les ajouter en cliquant sur le bouton en bas de l'écran."));
    } else {
      texts.add(Text(
          "Avec ${bibliotheque.nbLivres} livres, vous êtes culturellement plus riches..."));
      texts.add(Text(
          "Cependant, vous êtes aussi plus pauvres de ${bibliotheque.totalPrice.toStringAsFixed(2)}€ ! :)"));
    }

    var textContainer = CardPadding(
        title: bibliotheque.nom,
        child: Column(children: texts));

    var listView = BookList(books: bibliotheque.livres);

    return RefreshIndicator(
        onRefresh: () {
          var future =
              futureBibliotheque = bibliothequeService.fetchBibliotheque();
          loading = false;
          return future;
        },
        child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(children: [textContainer, listView])));
  }
}
