import 'package:bookshelf/consts.dart';
import 'package:bookshelf/models/livre.dart';
import 'package:bookshelf/services/livre_service.dart';
import 'package:bookshelf/widget/book_list.dart';
import 'package:flutter/material.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({super.key});

  @override
  State<SearchPage> createState() => SearchPageState();
}

class SearchPageState extends State<SearchPage> {

  var _value = "";
  late Future<List<Livre>> _futureLivre = Future.value([]);

  @override
  Widget build(BuildContext context) {
    var booklist = FutureBuilder<List<Livre>>(
        future: _futureLivre,
        builder: (context, snapshot) {
          if(snapshot.connectionState != ConnectionState.done){
            return const CircularProgressIndicator();
          }
          if (snapshot.hasData) {
            if (snapshot.data!.isEmpty && _value.isNotEmpty) {
              return const Text("Aucun résultat correspondant");
            }
            return BookList(books: snapshot.data!);
          }
          if (snapshot.hasError) {
            return Container(
                margin: defaultSpacing, child: Text(snapshot.error.toString()));
          }
          return Container();
        });

    var searchBar = Padding(
        padding: defaultSpacing,
        child: SearchBar(
          leading: const Icon(Icons.search),
          onChanged: search,
        ));
    return Scaffold(
        appBar: AppBar(
          title: const Text("Recherche"),
        ),
        body: Column(children: [
          searchBar,
          Expanded(child: SingleChildScrollView(child: booklist))
        ]));
  }

  search(String term) {
    _value = term;
    setState(() {
      _futureLivre = LivreService().search(term);
    });
  }
}
