import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeSettings with ChangeNotifier{

  ThemeSettings._privateConstructor();

  static final ThemeSettings _instance = ThemeSettings._privateConstructor();

  factory ThemeSettings() {
    return _instance;
  }

  Color _seedColor = Colors.deepPurple;
  Color get seedColor => _seedColor;
  ThemeData get theme => buildLightTheme(_seedColor);
  ThemeData get darkTheme => buildDarkTheme(_seedColor);

  ThemeMode _themeMode = ThemeMode.system;
  ThemeMode get themeMode => _themeMode;

  void loadPreferences() {
    SharedPreferences.getInstance().then((prefs) {
      var savedTheme = prefs.getInt("themeSeedColor");
      if(savedTheme != null){
        changeThemeColor(Color(savedTheme));
      }
      var savedMode = prefs.getInt("themeMode");
      if(savedMode != null){
        setThemeMode(ThemeMode.values[savedMode]);
      }
    });
  }

  void changeThemeColor(Color seedColor) {
    _seedColor = seedColor;
    SharedPreferences.getInstance().then((prefs) => prefs.setInt("themeSeedColor", seedColor.value));
    notifyListeners();
  }

  static buildLightTheme(Color colorSeed){
    return buildTheme(colorSeed, Brightness.light);
  }

  static buildDarkTheme(Color colorSeed){
    return buildTheme(colorSeed, Brightness.dark);
  }

  static buildTheme(Color colorSeed, Brightness brightness){
    return ThemeData(
      colorSchemeSeed: colorSeed,
      brightness: brightness,
      useMaterial3: true,
    );
  }

  void setThemeMode(ThemeMode themeMode) {
    _themeMode = themeMode;
    SharedPreferences.getInstance().then((prefs) => prefs.setInt("themeMode", themeMode.index));
    notifyListeners();
  }
}