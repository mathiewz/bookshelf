package com.mathiewz.bookshelf.dao;

import com.mathiewz.bookshelf.entity.Auteur;
import com.mathiewz.bookshelf.entity.Livre;
import com.mathiewz.bookshelf.entity.LivrePossede;
import com.mathiewz.bookshelf.entity.User;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Optional;

@ApplicationScoped
public class LivrePossedeDAO implements PanacheRepository<LivrePossede> {

    public Optional<LivrePossede> findByLivreAndUser(Livre livre, User user){
        var params = Parameters
                .with("livre", livre)
                .and("user", user);
        return find("livre = :livre and user = :user", params).firstResultOptional();
    }

}
