package com.mathiewz.bookshelf.dao;

import com.mathiewz.bookshelf.entity.Libelle;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Optional;

@ApplicationScoped
public class LibelleDAO implements PanacheRepository<Libelle> {

    public Optional<String> findForName(String name){
        return find("name", name).firstResultOptional().map(Libelle::getValue);
    }

}
