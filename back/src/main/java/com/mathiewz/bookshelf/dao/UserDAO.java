package com.mathiewz.bookshelf.dao;

import com.mathiewz.bookshelf.entity.User;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Optional;

@ApplicationScoped
public class UserDAO implements PanacheRepository<User> {

    public Optional<User> findByUsername(String username){
        return find("username", username).firstResultOptional();
    }
    public Optional<User> findByUsernameAndPassword(String username, String password){
        var params = Parameters
                .with("username", username)
                .and("password", password);
        return find("username = :username and password = :password", params).firstResultOptional();
    }

}
