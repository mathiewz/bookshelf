package com.mathiewz.bookshelf.dao;

import com.mathiewz.bookshelf.entity.Livre;
import com.mathiewz.bookshelf.entity.StatutLecture;
import com.mathiewz.bookshelf.entity.User;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Optional;

@ApplicationScoped
public class StatutLectureDAO implements PanacheRepository<StatutLecture> {

    public Optional<StatutLecture> findByLivreAndUser(Livre livre, User user){
        var params = Parameters
                .with("livre", livre)
                .and("user", user);
        return find("livre = :livre and user = :user", params).firstResultOptional();
    }

}
