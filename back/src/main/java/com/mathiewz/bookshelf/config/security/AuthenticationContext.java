package com.mathiewz.bookshelf.config.security;

import jakarta.enterprise.context.RequestScoped;

@RequestScoped
public class AuthenticationContext {
    private Long currentUserId;

    public Long getCurrentUserId() {
        return currentUserId;
    }

    public void setCurrentUserId(Long currentUserId) {
        this.currentUserId = currentUserId;
    }
}