package com.mathiewz.bookshelf.controller;

import com.mathiewz.bookshelf.dto.BookshelfDTO;
import com.mathiewz.bookshelf.dto.LivreDTO;
import com.mathiewz.bookshelf.service.BookService;
import com.mathiewz.bookshelf.service.BookshelfService;
import io.quarkus.security.Authenticated;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;

import java.util.List;

@Path("/bookshelf")
@Authenticated
public class BookshelfController {

    private final BookService bookService;
    private final BookshelfService bookshelfService;

    public BookshelfController(
            BookService bookService,
            BookshelfService bookshelfService
    ) {
        this.bookService = bookService;
        this.bookshelfService = bookshelfService;
    }

    @GET
    public BookshelfDTO getAll(){
        return bookshelfService.getAllLivres();
    }

    @PUT
    public BookshelfDTO updateLu(
            List<LivreDTO> livres
    ) {
        livres.forEach(livre -> bookService.readBook(livre.getId(), livre.isLu()));
        return getAll();
    }

}
