package com.mathiewz.bookshelf.controller;

import com.mathiewz.bookshelf.dto.UserDTO;
import com.mathiewz.bookshelf.service.UserService;
import io.quarkus.security.Authenticated;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

import java.util.List;

@Path("/users")
@Authenticated
public class UsersController {

    private final UserService userService;

    public UsersController(
            UserService userService
    ) {
        this.userService = userService;
    }

    @GET
    public List<UserDTO> getUsers(){
        return userService.getUsers();
    }

}
