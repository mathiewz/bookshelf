package com.mathiewz.bookshelf.exception;

public class UserAlreadyExistsException extends Exception {
    public UserAlreadyExistsException(String identifiant) {
        super("L'utilisateur avec l'identifiant "+identifiant+" existe déjà");
    }
}
