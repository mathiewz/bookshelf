package com.mathiewz.bookshelf.exception;

public class BadJwtSignatureException extends RuntimeException {
    public BadJwtSignatureException(String message) {
        super(message);
    }
}
