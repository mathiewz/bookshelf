package com.mathiewz.bookshelf.exception;

public class InvalidCodeException extends Exception {
    public InvalidCodeException(String code) {
        super("Code d'invitation invalide : "+code);
    }
}
