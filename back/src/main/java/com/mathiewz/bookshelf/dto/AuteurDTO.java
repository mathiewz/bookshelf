package com.mathiewz.bookshelf.dto;

import com.mathiewz.bookshelf.entity.Auteur;

public class AuteurDTO {

    private int id;
    private String nom;
    private String prenom;

    public AuteurDTO() {
        //Default constructor fon JSON deserialization
    }

    public AuteurDTO(Auteur auteur) {
        this.id = auteur.getId();
        this.nom = auteur.getNom();
        this.prenom = auteur.getPrenom();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

}
