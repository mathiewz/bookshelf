package com.mathiewz.bookshelf.dto;

import java.util.List;

public class BookshelfDTO {

    private final String nom;
    private final List<LivreDTO> livres;

    public BookshelfDTO(String nom, List<LivreDTO> livres){
        this.nom = nom;
        this.livres = livres;
    }

    public float getTotalPrice() {
        return livres.stream()
                .map(LivreDTO::getPrix)
                .reduce(Float::sum)
                .orElse(0f);
    }

    public String getNom() {
        return nom;
    }

    public int getNbLivres(){
        return livres.size();
    }

    public List<LivreDTO> getLivres() {
        return livres;
    }

}
