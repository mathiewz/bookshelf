package com.mathiewz.bookshelf.dto;

import com.mathiewz.bookshelf.entity.LivrePossede;

import java.math.BigInteger;
import java.time.LocalDate;

public record OwnerDTO(
        BigInteger userId,
        String displayName,
        LocalDate dateAjout

) {

    public OwnerDTO(LivrePossede livrePossede){
        this(livrePossede.getUser().getId(), livrePossede.getUser().getDisplayName(), livrePossede.getDateAjout());
    }

}
