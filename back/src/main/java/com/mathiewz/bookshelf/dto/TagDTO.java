package com.mathiewz.bookshelf.dto;

import com.mathiewz.bookshelf.entity.Tag;

public class TagDTO {

    private long id;
    private String label;
    private String icon;
    private Integer iconCode;
    private String color;

    public TagDTO() {
        //Default constructor fon JSON deserialization
    }

    public TagDTO(Tag tag) {
        this.id = tag.getId();
        this.color = tag.getColor();
        this.icon = tag.getIcon();
        this.iconCode = tag.getIconCode();
        this.label = tag.getLabel();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getIconCode() {
        return iconCode;
    }

    public void setIconCode(Integer iconCode) {
        this.iconCode = iconCode;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
