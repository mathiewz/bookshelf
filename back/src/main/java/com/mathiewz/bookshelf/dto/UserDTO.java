package com.mathiewz.bookshelf.dto;

import com.mathiewz.bookshelf.entity.User;

import java.math.BigInteger;

public class UserDTO {

    private BigInteger userId;
    private String displayName;

    public UserDTO() {
        //Default constructor fon JSON deserialization
    }

    public UserDTO(User user) {
        userId = user.getId();
        displayName = user.getDisplayName();
    }

    public BigInteger getUserId() {
        return userId;
    }

    public void setUserId(BigInteger userId) {
        this.userId = userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
