package com.mathiewz.bookshelf.dto;

import com.mathiewz.bookshelf.entity.Livre;
import com.mathiewz.bookshelf.entity.LivrePossede;

import java.util.ArrayList;
import java.util.List;

public class LivreFormDTO {

    private String id;
    private AuteurDTO auteur;
    private String titre;
    private String edition;
    private float prix;
    private List<UserDTO> detenteurs;
    private List<TagDTO> tags;
    private boolean pret;
    private boolean lu;

    public LivreFormDTO() {
        //Default constructor fon JSON deserialization
        detenteurs = new ArrayList<>();
        tags = new ArrayList<>();
    }

    public LivreFormDTO(Livre livre){
        this.id = String.valueOf(livre.getId());
        this.auteur = new AuteurDTO(livre.getAuteur());
        this.titre = livre.getTitre();
        this.edition = livre.getEdition().getNom();
        this.detenteurs = livre.getDetenteurs().stream().map(LivrePossede::getUser).map(UserDTO::new).toList();
        this.tags = livre.getTags().stream().map(TagDTO::new).toList();
        this.pret = livre.isPret();
        this.lu = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AuteurDTO getAuteur() {
        return auteur;
    }

    public void setAuteur(AuteurDTO auteur) {
        this.auteur = auteur;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public List<UserDTO> getDetenteurs() {
        return detenteurs;
    }

    public void setDetenteurs(List<UserDTO> detenteurs) {
        this.detenteurs = detenteurs;
    }

    public boolean isPret() {
        return pret;
    }

    public void setPret(boolean pret) {
        this.pret = pret;
    }

    public boolean isLu() {
        return lu;
    }

    public void setLu(boolean lu) {
        this.lu = lu;
    }

    public List<TagDTO> getTags() {
        return tags;
    }

    public void setTags(List<TagDTO> tags) {
        this.tags = tags;
    }
}
