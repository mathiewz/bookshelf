package com.mathiewz.bookshelf.dto;

public class InvitationDTO {

    private String code;
    private String identifiant;
    private String password;
    private String displayName;
    private String bibliothequeName;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getBibliothequeName() {
        return bibliothequeName;
    }

    public void setBibliothequeName(String bibliothequeName) {
        this.bibliothequeName = bibliothequeName;
    }
}
