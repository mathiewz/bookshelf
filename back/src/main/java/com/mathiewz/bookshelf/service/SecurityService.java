package com.mathiewz.bookshelf.service;

import com.mathiewz.bookshelf.config.security.AuthenticationContext;
import com.mathiewz.bookshelf.entity.AuthenticationRequest;
import com.mathiewz.bookshelf.entity.User;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.jwt.algorithm.SignatureAlgorithm;
import io.smallrye.jwt.build.Jwt;
import io.smallrye.jwt.util.KeyUtils;
import jakarta.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.crypto.SecretKey;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

@ApplicationScoped
public class SecurityService {

    private final SecretKey key;
    private final UserService userService;
    private final AuthenticationContext authenticationContext;

    public SecurityService(
            UserService userService,
            @ConfigProperty(name = "security.secret") String secret,
            AuthenticationContext authenticationContext
    ) {
        this.userService = userService;
        key = KeyUtils.createSecretKeyFromSecret(secret);
        this.authenticationContext = authenticationContext;
    }

    public String createToken(User user) {
        return Jwt.claim("id", user.getId())
                .claim("username", user.getUsername())
                .expiresAt(Instant.now().plus(6L, ChronoUnit.HOURS))
                .issuedAt(Instant.now())
                .jws().algorithm(SignatureAlgorithm.HS512)
                .sign(key);
    }

    public User authenticate(AuthenticationRequest request) {
        return userService.loadUserByUsernameAndPassword(request.getUsername(), request.getPassword());
    }

    public User getCurrentUser(){
        return userService.getUser(this.authenticationContext.getCurrentUserId());
    }

}
