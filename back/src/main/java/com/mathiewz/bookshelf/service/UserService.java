package com.mathiewz.bookshelf.service;

import com.mathiewz.bookshelf.dao.InviteCodeDAO;
import com.mathiewz.bookshelf.dao.UserDAO;
import com.mathiewz.bookshelf.dto.InvitationDTO;
import com.mathiewz.bookshelf.dto.UserDTO;
import com.mathiewz.bookshelf.entity.Bibliotheque;
import com.mathiewz.bookshelf.entity.InviteCode;
import com.mathiewz.bookshelf.entity.User;
import com.mathiewz.bookshelf.exception.InvalidCodeException;
import com.mathiewz.bookshelf.exception.NotFoundException;
import com.mathiewz.bookshelf.exception.UserAlreadyExistsException;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ApplicationScoped
public class UserService {

    private final UserDAO userDAO;
    private final InviteCodeDAO inviteCodeDAO;

    public UserService(
            UserDAO userDAO,
            InviteCodeDAO inviteCodeDAO
    ) {
        this.userDAO = userDAO;
        this.inviteCodeDAO = inviteCodeDAO;
    }

    public User loadUserByUsername(String username) {
        return userDAO.findByUsername(username.toUpperCase()).orElse(null);
    }

    public User loadUserByUsernameAndPassword(String username, String password) {
        return userDAO.findByUsernameAndPassword(username.toUpperCase(), password).orElse(null);
    }

    public User getUser(Long id) {
        return userDAO.findByIdOptional(id).orElseThrow(() -> new NotFoundException("Utilisateur d'id "+id+" inconnu"));
    }

    public List<UserDTO> getUsers() {
        return userDAO.listAll().stream()
                .map(UserDTO::new)
                .collect(Collectors.toList());
    }

    @Transactional
    public BigInteger newUser(InvitationDTO dto) throws InvalidCodeException, UserAlreadyExistsException {
        InviteCode invitation = getInvitation(dto.getCode())
                .orElseThrow(() -> new InvalidCodeException(dto.getCode()));
        if(loadUserByUsername(dto.getIdentifiant()) != null){
            throw new UserAlreadyExistsException(dto.getIdentifiant());
        }
        User user = buildUser(dto);
        userDAO.persist(user);
        invitation.setUsed(true);
        invitation.setUser(user);
        return user.getId();
    }

    private User buildUser(InvitationDTO dto) {
        User user = new User();
        user.setUsername(dto.getIdentifiant().toUpperCase());
        user.setPassword(dto.getPassword());
        user.setDisplayName(dto.getDisplayName());
        Bibliotheque bibliotheque = buildBibliotheque(dto);
        user.setBibliotheque(bibliotheque);
        bibliotheque.setUsers(Arrays.asList(user));
        return user;
    }

    private Bibliotheque buildBibliotheque(InvitationDTO dto) {
        Bibliotheque bibliotheque = new Bibliotheque();
        bibliotheque.setNom(dto.getBibliothequeName());
        return bibliotheque;
    }

    private Optional<InviteCode> getInvitation(String code) {
        return inviteCodeDAO.findByCodeAndUsedFalse(code);
    }
}
