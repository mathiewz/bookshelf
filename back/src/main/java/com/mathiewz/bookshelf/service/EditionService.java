package com.mathiewz.bookshelf.service;

import com.mathiewz.bookshelf.dao.EditionDAO;
import com.mathiewz.bookshelf.dto.LivreDTO;
import com.mathiewz.bookshelf.entity.Edition;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

@ApplicationScoped
public class EditionService {

    private final EditionDAO editionDAO;
    private final BookshelfService bookshelfService;

    public EditionService(EditionDAO editionDAO, BookshelfService bookshelfService) {
        this.editionDAO = editionDAO;
        this.bookshelfService = bookshelfService;
    }

    public Edition getOrCreateEdition(String nomEdition){
        return editionDAO.findByNom(nomEdition).orElseGet(() -> registerEdition(nomEdition));
    }

    private Edition registerEdition(String nomEdition) {
        Edition edition = new Edition();
        edition.setNom(nomEdition);
        editionDAO.persist(edition);
        return edition;
    }

    public String getCurrentUserFavorite() {
        Map<String, Long> editionCount =  bookshelfService.getLivresForCurrentUser().stream()
                .collect(Collectors.groupingBy(LivreDTO::getEdition, Collectors.counting()));

        return Collections.max(editionCount.entrySet(), Map.Entry.comparingByValue()).getKey();
    }
}
