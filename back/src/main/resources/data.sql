create table editions
(
    id  serial primary key,
    nom text not null unique
);

create table libelles
(
    name    varchar(255) not null primary key,
    libelle varchar(255) not null
);

create table users
(
    id           serial primary key,
    username     text    not null,
    password     text    not null,
    display_name text null
);

create table auteurs
(
    id     serial,
    nom    text not null,
    prenom text null,
    constraint auteurs_pk
        primary key (id)
);

create table livres
(
    id           serial primary key,
    titre        text  null,
    edition      text  null,
    prix         float(2) null,
    pret         int      null,
    detenteur_id int      null,
    edition_id   int      null,
    auteur_id    int      null,
    constraint livres_detenteur_id_fk foreign key (detenteur_id) references users (id),
    constraint livres_edition_id_fk foreign key (edition_id) references editions (id),
    constraint livres_auteur_id_fk foreign key (auteur_id) references auteurs (id)
);

create table books_reader
(
    livre   int null,
    lecteur int null,
    constraint books_reader_livres_id_fk foreign key (livre) references livres (id),
    constraint books_reader_users_id_fk foreign key (lecteur) references users (id)
);

INSERT INTO libelles (name, libelle)
VALUES ('addBook.title', 'Chouette ! Un nouveau livre !');
INSERT INTO libelles (name, libelle)
VALUES ('editBook.title', 'Ah ? Une coquille à corriger ?');
INSERT INTO libelles (name, libelle)
VALUES ('title', 'La bibliothèque de Developpement');

INSERT INTO users (username, password, display_name)
VALUES ('ADMIN',
        'ee885ec57005466b02254a19a29aff2adb56d81eb7972eca58cb28d52f75b2f5b5fad562a89b7ba4a48491ec017d3ccc0eb8556540a680a5f63b1922c6fb0eaf',
        'Administrateur');
INSERT INTO users (username, password, display_name)
VALUES ('DEV',
        '941596d73a9594bc48547d120807a9cb1b6837b2238fc3820fc752ed35dcbe256b4e75d18fd64a016e7c59d29869ab7ed8986b6c0fa5291dce55d2c573a305a4',
        'Developpeur');

INSERT INTO editions (nom)
VALUES ('Editions pipeau');

INSERT INTO auteurs (prenom, nom)
VALUES ('John', 'Doe');
INSERT INTO auteurs (prenom, nom)
VALUES ('Jane', 'Test');

INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Nom du livre', 'Editions pipeau', 14.9, 0, 1, 1, 1);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Autre livre tome 1', 'Editions pipeau', 15.5, 1, 2, 1, 2);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Autre livre tome 2', 'Editions pipeau', 15.5, 1, 2, 1, 2);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Autre livre tome 3', 'Editions pipeau', 15.5, 1, 2, 1, 2);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Autre livre tome 4', 'Editions pipeau', 15.5, 1, 2, 1, 2);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Autre livre tome 5', 'Editions pipeau', 15.5, 1, 2, 1, 2);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Autre livre tome 6', 'Editions pipeau', 15.5, 1, 2, 1, 2);


INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Lorem ipsum 1', 'Editions pipeau', 15.5, 1, 2, 1, 2);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Lorem ipsum 2', 'Editions pipeau', 15.5, 1, 2, 1, 2);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Lorem ipsum 3', 'Editions pipeau', 15.5, 1, 2, 1, 2);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Lorem ipsum 4', 'Editions pipeau', 15.5, 1, 2, 1, 2);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Lorem ipsum 5', 'Editions pipeau', 15.5, 1, 2, 1, 2);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Lorem ipsum 6', 'Editions pipeau', 15.5, 1, 2, 1, 2);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Lorem ipsum 7', 'Editions pipeau', 15.5, 1, 2, 1, 2);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Lorem ipsum 8', 'Editions pipeau', 15.5, 1, 2, 1, 2);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Lorem ipsum 9', 'Editions pipeau', 15.5, 1, 2, 1, 2);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Lorem ipsum 10', 'Editions pipeau', 15.5, 1, 2, 1, 2);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Lorem ipsum 11', 'Editions pipeau', 15.5, 1, 2, 1, 2);
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Lorem ipsum 12', 'Editions pipeau', 15.5, 1, 2, 1, 2);


INSERT INTO books_reader(livre, lecteur)
VALUES (1, 2);

create table bibliotheque
(
    id  serial primary key,
    nom text null
);

create table bibliotheque_livre
(
    livre        int null,
    bibliotheque int null,
    constraint bibliotheque_livre_livres_id_fk foreign key (livre) references livres (id),
    constraint bibliotheque_livre_bibliotheques_id_fk foreign key (bibliotheque) references bibliotheque (id)
);

create table bibliotheque_user
(
    bibliotheque int null,
    user_id      int null,
    constraint bibliotheque_user_livres_id_fk foreign key (bibliotheque) references bibliotheque (id),
    constraint bibliotheque_user_users_id_fk foreign key (user_id) references users (id)
);

insert into bibliotheque(nom)
select libelle
from libelles
where name = 'title';
delete
from libelles
where name = 'title';
insert into bibliotheque_livre(livre, bibliotheque)
select livres.id, bibliotheque.id
from livres,
     bibliotheque;
insert into bibliotheque_user(user_id, bibliotheque)
select users.id, bibliotheque.id
from users,
     bibliotheque;
-- not in prod
alter table users
    add column bibliotheque int;
update users
set bibliotheque = (select bibliotheque from bibliotheque_user where bibliotheque_user.user_id = users.id)
where true;
alter table users
    add constraint users_bibliotheque_id_fk foreign key (bibliotheque) references bibliotheque (id);
drop table bibliotheque_user;

create table livre_statut
(
    id      serial primary key,
    livre   int,
    user_id int,
    statut  text,
    constraint livre_statut_livres_id_fk foreign key (livre) references livres (id),
    constraint livre_statut_user_id_fk foreign key (user_id) references users (id)
);

insert into livre_statut (livre, user_id, statut)
select livre, lecteur, 'LU'
from books_reader;
drop table books_reader;

INSERT INTO users (username, password, display_name)
VALUES ('DEV2',
        '941596d73a9594bc48547d120807a9cb1b6837b2238fc3820fc752ed35dcbe256b4e75d18fd64a016e7c59d29869ab7ed8986b6c0fa5291dce55d2c573a305a4',
        'Developpeur 2');
INSERT INTO livres (titre, edition, prix, pret, detenteur_id, edition_id, auteur_id)
VALUES ('Autre livre tome 7', 'Editions pipeau', 15.5, 1, 3, 1, 2);

create table book_owners
(
    livre   int not null,
    user_id int not null,
    constraint book_owners_livres_id_fk foreign key (livre) references livres (id),
    constraint book_owners_user_id_fk foreign key (user_id) references users (id)
);

INSERT into book_owners
select id, detenteur_id
from livres;

drop table bibliotheque_livre;
ALTER TABLE livres DROP constraint livres_detenteur_id_fk ;
alter table livres drop column detenteur_id;

create table invite_code
(
    id      serial primary key,
    code    text,
    used    boolean,
    user_id int
);

insert into invite_code(code, used)
values ('ABC', false),
       ('BCD', false);

-- new

create table tags
(
    id serial primary key,
    label text,
    icon text,
    color text
);

insert into tags (label, icon, color) values ('Bande dessinée', 'faComment', 'purple');
insert into tags (label, icon, color) values ('Roman', 'faBook', 'blue');

create table book_tags
(
    livre   int not null,
    tag int not null,
    constraint book_tags_livres_id_fk foreign key (livre) references livres (id),
    constraint book_tags_tags_id_fk foreign key (tag) references tags (id)
);

insert into book_tags (livre, tag) SELECT livres.id, 1 from livres where titre like 'Autre livre%';

insert into tags (label, icon, color) values ('Nouvelles', 'faReceipt', 'orange');
insert into tags (label, icon, color) values ('Poésie', 'faFeatherAlt', 'green');

alter table tags add column icon_code_point int;

update tags set icon_code_point = 61557 where icon = 'faComment';
update tags set icon_code_point = 61485 where icon = 'faBook';
update tags set icon_code_point = 62787 where icon = 'faReceipt';
update tags set icon_code_point = 62827 where icon = 'faFeatherAlt';

ALTER TABLE livres ALTER pret TYPE bool USING CASE WHEN pret=0 THEN FALSE ELSE TRUE END;

alter table book_owners ADD COLUMN date_ajout date;
alter table book_owners ADD COLUMN id serial primary key;

update book_owners bo set date_ajout = date '2016-02-13' + bo.livre;