import {Themes} from "./src/app/config/themes";

module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {},
  },
  plugins: [require("daisyui")],
  daisyui: {
    themes: [Themes.lightTheme, Themes.darkTheme],
  },
}
