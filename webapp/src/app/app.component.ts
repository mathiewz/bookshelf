import {Component, OnInit, Renderer2} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {environment} from '../environments/environment';
import {ThemeService} from './services/theme.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  private baseTitle: string;
  private pageTitle: string;

  public constructor(
    private titleService: Title,
    private themeService: ThemeService,
    renderer: Renderer2
  ) {
    this.themeService.renderer = renderer;
  }

  ngOnInit(): void {
    if (environment.gaTrackingId) {
      // register google tag manager
      const gTagManagerScript = document.createElement('script');
      gTagManagerScript.async = true;
      gTagManagerScript.src = `https://www.googletagmanager.com/gtag/js?id=${environment.gaTrackingId}`;
      document.head.appendChild(gTagManagerScript);

      // register google analytics
      const gaScript = document.createElement('script');
      gaScript.innerHTML = `
      window.dataLayer = window.dataLayer || [];
      function gtag() { dataLayer.push(arguments); }
      gtag('js', new Date());
      gtag('config', '${environment.gaTrackingId}');
    `;
      document.head.appendChild(gaScript);
      console.log('Analytics : ON')
    } else {
      console.log('Analytics : OFF')
    }

    this.setBaseTitle('Bibliotheque');
    this.themeService.loadTheme();
  }

  public setBaseTitle(title: string) {
    this.baseTitle = title;
    this.setTitle(this.pageTitle)
  }

  public setTitle(newTitle? : string) {
    this.pageTitle = newTitle;
    if(newTitle){
      this.titleService.setTitle( `${newTitle} - ${this.baseTitle}` );
    } else {
      this.titleService.setTitle(this.baseTitle)
    }
  }
}
