import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BookshelfComponent} from './components/bookshelf/bookshelf.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {AjoutLivreComponent} from './components/livres/ajout-livre/ajout-livre.component';
import {LoggedGuard} from './guards/logged-guard.service';
import {EditLivreComponent} from './components/livres/edit-livre/edit-livre.component';
import {BookViewComponent} from './components/livres/view/book-view.component';
import {DashboardComponent} from './components/user/dashboard/dashboard.component';
import {LoginComponent} from './components/login/login.component';
import {NoauthGuard} from './guards/noauth-guard.service';
import {InviteComponent} from './components/invite/invite.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'bookshelf'
  },
  {
    path: 'bookshelf',
    pathMatch: 'full',
    component: BookshelfComponent,
    canActivate: [LoggedGuard]
  },
  {
    path: 'login',
    pathMatch: 'full',
    component: LoginComponent,
    canActivate: [NoauthGuard]
  },
  {
    path: 'book/new',
    pathMatch: 'full',
    component: AjoutLivreComponent,
    canActivate: [LoggedGuard]
  },
  {
    path: 'book/:id',
    pathMatch: 'full',
    component: BookViewComponent,
  },
  {
    path: 'book/:id/edit',
    pathMatch: 'full',
    component: EditLivreComponent,
    canActivate: [LoggedGuard]
  },
  {
    path: 'user/dashboard',
    pathMatch: 'full',
    component: DashboardComponent,
    canActivate: [LoggedGuard]
  },
  {
    path: 'invite/:code',
    pathMatch: 'full',
    component: InviteComponent,
    canActivate: [NoauthGuard]
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
