import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, provideHttpClient, withInterceptorsFromDi} from '@angular/common/http';
import {ApiInterceptor} from './config/api-interceptor.service';
import {BookshelfComponent} from './components/bookshelf/bookshelf.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSortModule} from '@angular/material/sort';
import {AuthInterceptor} from './config/auth-interceptor.service';
import {LoginComponent} from './components/login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HeaderComponent} from './components/layout/header/header.component';
import {AjoutLivreComponent} from './components/livres/ajout-livre/ajout-livre.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {BookFormComponent} from './components/livres/form/book-form.component';
import {EditLivreComponent} from './components/livres/edit-livre/edit-livre.component';
import {BookViewComponent} from './components/livres/view/book-view.component';
import {FaIconComponent} from '@fortawesome/angular-fontawesome';
import {SpinnerComponent} from './components/utils/spinner/spinner.component';
import {SearchBarComponent} from './components/layout/header/search-bar/search-bar/search-bar.component';
import {DashboardComponent} from './components/user/dashboard/dashboard.component';
import {BooklistComponent} from './components/bookshelf/booklist/booklist.component';
import {ButtonComponent} from './components/utils/button/button.component';
import {InviteComponent} from './components/invite/invite.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {BoldPipe} from './pipes/bold.pipe';
import {BadgeComponent} from './components/utils/badge/badge.component';
import {ThemeToggleComponent} from './components/layout/header/theme-toggle/theme-toggle.component';
import {NgOptimizedImage} from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    BookshelfComponent,
    PageNotFoundComponent,
    LoginComponent,
    HeaderComponent,
    AjoutLivreComponent,
    BookFormComponent,
    EditLivreComponent,
    BookViewComponent,
    SpinnerComponent,
    SearchBarComponent,
    DashboardComponent,
    BooklistComponent,
    BooklistComponent,
    ButtonComponent,
    InviteComponent,
    BoldPipe,
    BadgeComponent,
    ThemeToggleComponent,
    ThemeToggleComponent
  ],
  bootstrap: [AppComponent], imports: [BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSortModule,
    FormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    ReactiveFormsModule,
    MatExpansionModule, FaIconComponent, NgOptimizedImage], providers: [
    {provide: HTTP_INTERCEPTORS, multi: true, useClass: ApiInterceptor},
    {provide: HTTP_INTERCEPTORS, multi: true, useClass: AuthInterceptor},
    provideHttpClient(withInterceptorsFromDi())
  ]
})
export class AppModule {
}
