export class User {
  userId: number;
  displayName: string;

  constructor(data: any) {
    if(data == null){
      return;
    }
    this.userId = data.userId;
    this.displayName = data.displayName;
  }
}
