import {User} from './user';
import {Auteur} from './auteur';
import {Tag} from './tag';

export class Livre {
  id: string;
  auteur: Auteur;
  titre: string;
  edition: string;
  prix: number;
  detenteurs: User[];
  tags: Tag[]
  pret: boolean;
  lu: boolean;

  constructor(data: any) {
    if(data == null){
      return;
    }
    this.id = data.id;
    this.auteur = new Auteur(data.auteur);
    this.titre = data.titre;
    this.edition = data.edition;
    this.prix = data.prix;
    this.detenteurs = data.detenteurs?.map(user => new User(user)) ?? [];
    this.tags = data.tags?.map(tag => new Tag(tag)) ?? [];
    this.pret = data.pret;
    this.lu = data.lu;
  }

  getOwners(): string{
    return this.detenteurs.map(user => user.displayName).join(' & ')
  }

  ownedBy(user: User): boolean{
    return this.detenteurs.some(owner => owner.userId === user.userId);
  }

}
