import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {Tag} from '../model/tag';

@Injectable({
  providedIn: 'root'
})
export class ParameterService {

  constructor(private httpClient: HttpClient) { }
  getTags() : Observable<Tag[]>{
    return this.httpClient.get<Tag[]>('params/tags').pipe(map(tags => tags.map(tag => new Tag(tag))));
  }
}
