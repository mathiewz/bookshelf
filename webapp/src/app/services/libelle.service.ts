import { Injectable } from '@angular/core';
import {Bookshelf} from "../model/bookshelf";
import { HttpClient, HttpParams } from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LibelleService {

  constructor(private httpClient: HttpClient) { }

  public getLibelle(name: string): Observable<string>{
    // Initialize Params Object
    let params = new HttpParams();

    // Begin assigning parameters
    params = params.append('name', name);
    return this.httpClient.get('libelle', {responseType: 'text', params: params});
  }
}
