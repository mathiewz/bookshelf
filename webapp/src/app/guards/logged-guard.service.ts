import { Injectable } from '@angular/core';
import { Router, UrlTree } from "@angular/router";
import {AuthService} from "../services/auth/auth.service";

@Injectable({
  providedIn: 'root'
})
export class LoggedGuard {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate(): boolean | UrlTree {
    if(this.authService.isLogged()){
      return true;
    }
    return this.router.parseUrl('/login');
  }
}
