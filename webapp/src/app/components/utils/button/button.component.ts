import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IconDefinition} from '@fortawesome/fontawesome-svg-core';

@Component({
  selector: 'button-icon',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() icon: IconDefinition;
  @Input() classes: string[];
  @Input() disabled: boolean;

  @Output() onClick = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
