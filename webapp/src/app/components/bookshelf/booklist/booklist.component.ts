import {Component, inject, input, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Livre} from '../../../model/livre';
import {faCheck, faEye, faPen} from '@fortawesome/free-solid-svg-icons';
import {Sort} from '@angular/material/sort';
import {AuthService} from '../../../services/auth/auth.service';

@Component({
  selector: 'app-booklist',
  templateUrl: './booklist.component.html',
  styleUrls: ['./booklist.component.scss']
})
export class BooklistComponent implements OnInit, OnChanges {

  @Input() livres : Livre[];

  checkboxes = input(false);
  read = input(true);
  author = input(true);
  title = input(true);
  edition = input(true);
  price = input(true);
  owner = input(true);
  lend = input(true);
  view = input(true);
  edit = input(false);

  private authService = inject(AuthService);

  faCheck = faCheck;
  faEye = faEye;
  faPen = faPen;

  defaultSorting: Livre[];
  checkedBooks: string[];
  logged: boolean;

  ngOnInit(): void {
    this.logged = this.authService.isLogged();
    this.checkedBooks = [];
    this.defaultSorting = this.livres;
  }

  ngOnChanges(_: SimpleChanges): void {
    this.defaultSorting = this.livres;
  }

  updateCheckList(id: string, checked: boolean) {
    if(checked){
      this.checkedBooks.push(id);
    } else {
      this.checkedBooks = this.checkedBooks.filter(item => item !== id);
    }
  }

  sortData(sort: Sort) {
    const data = this.livres.slice();
    if (!sort.active || sort.direction === '') {
      this.livres = this.defaultSorting;
      return;
    }

    this.livres = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'auteur': return this.compare(a.auteur.nom, b.auteur.nom, isAsc);
        case 'titre': return this.compare(a.titre, b.titre, isAsc);
        case 'edition': return this.compare(a.edition, b.edition, isAsc);
        case 'prix': return this.compare(a.prix, b.prix, isAsc);
        case 'detenteur': return this.compare(a.getOwners(), b.getOwners(), isAsc);
        case 'pret': return this.compare(a.pret, b.pret, isAsc);
        case 'lu': return this.compare(a.lu, b.lu, isAsc);
        default: return 0;
      }
    });
  }

  compare(a: any, b: any, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
}
