import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {faMoon, faSun} from '@fortawesome/free-solid-svg-icons';
import {Themes} from '../../../../config/themes';
import {ThemeService} from '../../../../services/theme.service';

@Component({
  selector: 'app-theme-toggle',
  templateUrl: './theme-toggle.component.html',
  styleUrls: ['./theme-toggle.component.scss']
})
export class ThemeToggleComponent implements OnInit, AfterViewInit {

  constructor(
    private themeService: ThemeService
  ) { }

  @ViewChild('themeToggle') themeToggle: ElementRef;
  @Input() showLabel = false;

  faSun = faSun;
  faMoon = faMoon;
  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.themeToggle.nativeElement.checked = this.themeService.getCurrentTheme() === Themes.darkTheme;
  }

  updateTheme() {
    const isDarkMode = this.themeToggle.nativeElement.checked
    this.themeService.setTheme(isDarkMode ? Themes.darkTheme : Themes.lightTheme);
  }

  toggleCheck() {
    this.themeToggle.nativeElement.checked = !this.themeToggle.nativeElement.checked;
    this.updateTheme();
  }
}
