import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {debounceTime, filter, map, mergeMap} from 'rxjs/operators';
import {NavigationStart, Router} from '@angular/router';
import {BookService} from '../../../../../services/book/book.service';
import {Livre} from '../../../../../model/livre';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  searchControl: FormControl;
  searchResults: Livre[] = [];

  constructor(
    private bookService: BookService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.searchControl = new FormControl();
    this.searchControl.setValue('');
    this.searchControl.valueChanges.pipe(
      debounceTime(500),
      map(() => this.searchControl.value),
      mergeMap(search => this.bookService.search(search))
    ).subscribe(
      res => this.searchResults = res,
      err => console.error(err)
    );

    this.router.events.pipe(
      filter(event => event instanceof NavigationStart)
    ).subscribe(() => {
      this.searchControl.setValue('');
      this.searchResults = [];
    });
  }

}
