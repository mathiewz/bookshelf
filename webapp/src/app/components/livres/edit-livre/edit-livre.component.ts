import { Component, OnInit } from '@angular/core';
import {LibelleService} from "../../../services/libelle.service";
import {BookService} from "../../../services/book/book.service";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {AppComponent} from "../../../app.component";
import {Livre} from "../../../model/livre";
import {switchMap} from "rxjs/operators";

@Component({
  selector: 'app-edit-livre',
  templateUrl: './edit-livre.component.html',
  styleUrls: ['./edit-livre.component.scss']
})
export class EditLivreComponent implements OnInit {

  pageTitle: string;
  livre: Livre;

  constructor(
    private libelleService: LibelleService,
    private bookService: BookService,
    private router: Router,
    private appComponent: AppComponent,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.libelleService.getLibelle('editBook.title').subscribe(
      res => this.pageTitle = res,
      error => console.error(error)
    );
    this.appComponent.setTitle('Edition livre');
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.bookService.getBook(params.get('id')))
    ).subscribe(
      res => this.livre = res,
      error => {
        console.error(error);
        this.router.navigateByUrl('/404');
      }
    );
  }

  editBook(livre: Livre) {
    this.bookService.editBook(this.livre.id, livre).subscribe(
      res => this.router.navigateByUrl('/book/'+res.id),
      error => console.error(error)
    )
  }

}
