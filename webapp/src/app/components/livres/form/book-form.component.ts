import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserService} from '../../../services/user/user.service';
import {User} from '../../../model/user';
import {Livre} from '../../../model/livre';
import {Location} from '@angular/common';
import {Tag} from '../../../model/tag';
import {ParameterService} from '../../../services/parameter.service';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss']
})
export class BookFormComponent implements OnInit {

  @Input() livre :Livre;
  @Input() addOnCreate: boolean;

  @Input() autocompletedLivre: Livre;

  @Output() livreChange = new EventEmitter<Livre>();
  @Output() addOnCreateChange = new EventEmitter<boolean>();
  users: User[];
  tags: Tag[];

  constructor(
    private userService: UserService,
    private parameterService: ParameterService,
    private location: Location
  ) { }

  ngOnInit() {
    this.userService.getUsers().subscribe(
      res => this.users = res,
      error => console.error(error)
    );
    this.parameterService.getTags().subscribe(
      res => this.tags = res,
      error => console.error(error)
    );
    this.autocompletedLivre = new Livre({});
  }

  emitBook() {
    this.livreChange.emit(this.livre);
  }

  changeAddOnCreate() {
    this.addOnCreate = !this.addOnCreate
    this.addOnCreateChange.emit(this.addOnCreate);
  }

  cancel() {
    this.location.back(); // <-- go back to previous location on cancel
  }

  contains(tag: Tag) {
    return this.livre.tags.some(t => t.id === tag.id);
  }

  updateTag(tag: Tag, value: boolean) {
    if(value){
      this.livre.tags.push(tag);
    } else {
      this.livre.tags = this.livre.tags.filter(t => t.id !== tag.id);
    }
  }
}
